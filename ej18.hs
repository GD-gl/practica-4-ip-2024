-- CODIGO DEL EJERCICIO 8
cantDigitosInterno :: Integer -> Integer
cantDigitosInterno 0 = 1
cantDigitosInterno n = 1 + cantDigitosInterno (n `div` 10)

cantDigitos :: Integer -> Integer
cantDigitos n | n < 0 = undefined
              | n == 0 = 1
              | otherwise = cantDigitosInterno (n `div` 10)

-- ejercicio 18:

mayorDigitoParInterno :: Integer -> Integer -> Integer -> Integer
mayorDigitoParInterno n cifra maxPar | cifra > cantDigitos n = maxPar
                                     | (n `div` 10^(cifra-1)) `mod` 2 == 0 = mayorDigitoParInterno n (cifra+1) (max ((n `div` 10^(cifra-1)) `mod` 10) maxPar)
                                     | otherwise = mayorDigitoParInterno n (cifra+1) maxPar

mayorDigitoPar :: Integer -> Integer
mayorDigitoPar n = mayorDigitoParInterno n 1 (-1)

main = do
    print (mayorDigitoPar 20) -- 2
    print (mayorDigitoPar 10) -- 0
    print (mayorDigitoPar 11) -- -1
    print (mayorDigitoPar 75931) -- -1
    print (mayorDigitoPar 345646234) -- 6
    print (mayorDigitoPar 1212331238) -- 8