sumatoriaPotenciaB :: Int -> Int -> Int -> Int
sumatoriaPotenciaB q a 1 = q^(a+1)
sumatoriaPotenciaB q a b = (q^(a+b)) + sumatoriaPotenciaB q a (b-1)

sumatoriaPotenciaAB :: Int -> Int -> Int -> Int
sumatoriaPotenciaAB q 1 b = sumatoriaPotenciaB q 1 b
sumatoriaPotenciaAB q a b = sumatoriaPotenciaAB q (a-1) b + sumatoriaPotenciaB q a b

sumaPotencias :: Int -> Int -> Int -> Int
sumaPotencias q n m | q < 1 || n < 1 || m < 1 = undefined
                    | otherwise = sumatoriaPotenciaAB q n m

main = do
    print (sumaPotencias 2 3 2) -- 2² + 2³ + 2⁴ + 2³ + 2⁴ + 2⁵ = 84
    print (sumaPotencias 1 3 9) -- 1² + 1³ + 1⁴ + 1³ + 1⁴ + 1⁵ = 27
    print (sumaPotencias 3 4 2) -- 3² + 3³  + 3⁴ + 3⁵ + 3³ + 3⁴ + 3⁵ + 3⁶ = 1440