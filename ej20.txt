problema tomaValorMax(n1, n2: Z): Z {
    requiere: {n1 >= 1 && n2 >= n1}
    asegura: {res = m <=> sumaDivisores(m) == máx{sumaDivisores(i) | n1 <= i <= n2}}
}

-- Asumo: sumaDivisores incluye el 1 y n