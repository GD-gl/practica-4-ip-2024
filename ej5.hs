medioFactInterno :: Int -> Int -> Int
medioFactInterno 0 ntotal = ntotal
medioFactInterno iter ntotal = (ntotal - (2 * iter)) * medioFactInterno (iter - 1) ntotal

-- aca si puedo usar div, supongo
medioFact :: Int -> Int
medioFact n | n < 0 = undefined -- no quiero enteros
            | n == 0 = 1 -- caso 1 es importante, sino la recursión hace iteraciones negativos
            | otherwise = medioFactInterno ((n - 1) `div` 2) n

main = do
    print (medioFact 0) -- 1 
    print (medioFact 9) -- 945
    print (medioFact 10) -- 3840