-- si n es par retorna cero, sino retorna n
ceroSiEsPar :: Int -> Int
ceroSiEsPar n | n `mod` 2 == 0 = 0
              | otherwise = n

-- esto suma n si n es impar
sumaImparesInterno :: Int -> Int
sumaImparesInterno 0 = 0
sumaImparesInterno n = ceroSiEsPar n + sumaImparesInterno (n - 1)

-- esto ejecuta sumaImparesInterno con n * 2
-- sumaImparesInterno se saltea los pares por lo que voy a sumar n numeros
sumaImpares :: Int -> Int
sumaImpares n | n <= 0 = undefined -- no quiero enteros
              | otherwise = sumaImparesInterno (n + n)

main = do
    print (sumaImpares 3) -- 1+3+5 = 9
    print (sumaImpares 5) -- 1+3+5+7+9 = 25
    print (sumaImpares 8) -- 1+3+5+7+9+11+13+15 = 64