-- Debería usar Float por la materia en sí
-- pero voy a usar double para asegurar una precisión superior
factorial :: Int -> Int
factorial 0 = 1
factorial d = d * factorial (d-1)

-- uso fromIntegral para transformar un int en un double
eAproxInterno :: Int -> Double
eAproxInterno 0 = 1
eAproxInterno n = (1 / fromIntegral (factorial n)) + eAprox (n - 1)

eAprox :: Int -> Double
eAprox n | n < 0 = undefined
         | otherwise = eAproxInterno n

main = do
    print (eAprox 50) -- 2.71828182845.... (e)