sumaDivisoresInterno :: Integer -> Integer -> Integer
sumaDivisoresInterno n iter | n == iter = n
                            | n `mod` iter == 0 = iter + sumaDivisoresInterno n (iter+1)
                            | otherwise = sumaDivisoresInterno n (iter+1)

sumaDivisores :: Integer -> Integer
sumaDivisores n | n < 1 = undefined
                | otherwise = sumaDivisoresInterno n 1

tomaValorMaxInterno :: Integer -> Integer -> Integer -> Integer
tomaValorMaxInterno maxInclusivo iter valorMaxM | iter > maxInclusivo = valorMaxM
                                                | sumaDiv > valorMax = tomaValorMaxInterno maxInclusivo (iter+1) sumaDivM
                                                | otherwise = tomaValorMaxInterno maxInclusivo (iter+1) valorMaxM
                                                where sumaDiv = sumaDivisores iter
                                                      valorMax = sumaDivisores valorMaxM
                                                      sumaDivM = iter

tomaValorMax :: Integer -> Integer -> Integer
tomaValorMax n1 n2 | not(n1 >= 1 && n2 >= n1) = undefined
                   | otherwise = tomaValorMaxInterno n2 n1 1

main = do
    print (sumaDivisores 10) -- 1 + 2 + 5 + 10 = 18
    print (sumaDivisores 3) -- 4

    print (tomaValorMax 100 111) -- 108
    print (tomaValorMax 100 107) -- 100

    print (sumaDivisores 100)
    print (sumaDivisores 108)
    print (sumaDivisores 111)