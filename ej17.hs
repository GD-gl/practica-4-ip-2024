-- CÓDIGO DEL EJERCICIO 1
fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n | n >= 0 = fib (n-1) + fib(n-2)
      | otherwise = undefined

esFibonacciInterno :: Integer -> Integer -> Integer -> Bool
esFibonacciInterno n prevFib prevFibN | n == prevFib = True
                                      | prevFib > n = False
                                      | otherwise = esFibonacciInterno n (fib fibN) fibN
                                      where fibN = prevFibN + 1

esFibonacci :: Integer -> Bool
esFibonacci n = esFibonacciInterno n (fib 0) 0

main = do
    print (esFibonacci 0) -- True
    print (esFibonacci 1) -- True
    print (esFibonacci 2) -- True
    print (esFibonacci 3) -- True
    print (esFibonacci 5) -- True
    print (esFibonacci 4) -- False
    print (esFibonacci 8) -- True
    print (esFibonacci 6) -- False
    print (esFibonacci 13) -- True
    print (esFibonacci 14) -- False
    print (esFibonacci 21) -- True
    print (esFibonacci 22) -- False
    print (esFibonacci 34) -- True
    print (esFibonacci 33) -- False
    print (esFibonacci 55) -- True
    print (esFibonacci 53) -- False
    print (esFibonacci 89) -- True
    print (esFibonacci 88) -- False