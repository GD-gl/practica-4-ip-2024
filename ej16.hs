menorDivisorInterno :: Integer -> Integer -> Integer
menorDivisorInterno n iter | iter < 2 = undefined -- 2 para adelante
                           | n == 1 = 1
                           | iter == n = n
                           | n `mod` iter == 0 = iter
                           | otherwise = menorDivisorInterno n (iter+1)

menorDivisor :: Integer -> Integer
menorDivisor n | n < 1 = undefined
               | otherwise = menorDivisorInterno n 2

esPrimo :: Integer -> Bool
esPrimo n = menorDivisor n == n

siguientePrimo :: Integer -> Integer
siguientePrimo n | esPrimo m = m
                 | otherwise = siguientePrimo m
                 where m = n + 1

nEsimoPrimoInterno :: Integer -> Integer -> Integer
nEsimoPrimoInterno previo iter | iter == 1 = siguientePrimo previo
                               | otherwise = nEsimoPrimoInterno (siguientePrimo previo) (iter - 1)

nEsimoPrimo :: Integer -> Integer
nEsimoPrimo n | n < 1 = undefined
              | otherwise = nEsimoPrimoInterno 1 n

noSonCoprimosInterno :: Integer -> Integer -> Integer -> Bool
noSonCoprimosInterno n m iter | iter < 2 = undefined -- 2 para adelante
                            | n == 1 || m == 1 = False
                            | iter == n || iter == m = False -- no se encontraron divisores comunes
                            | n `mod` iter == 0 && m `mod` iter == 0 = True
                            | otherwise = noSonCoprimosInterno n m (iter+1)

sonCoprimos :: Integer -> Integer -> Bool
sonCoprimos n m | n < 1 || m < 1 = undefined
                | otherwise = not (noSonCoprimosInterno n m 2)


main = do
    print (menorDivisor 2) -- 2
    print (menorDivisor 3) -- 3
    print (menorDivisor 5) -- 5
    print (menorDivisor 7) -- 7
    print (menorDivisor 11) -- 11
    print (menorDivisor 15) -- 3
    print (menorDivisor 18) -- 2
    print (menorDivisor 121) -- 11

    print (esPrimo 11) -- True
    print (esPrimo 12) -- False

    print (sonCoprimos 10 20) -- False
    print (sonCoprimos 7 11) -- True
    print (sonCoprimos 7 9) -- True
    print (sonCoprimos 11 14) -- True

    print (nEsimoPrimo 1) -- 2
    print (nEsimoPrimo 2) -- 3
    print (nEsimoPrimo 3) -- 5
    print (nEsimoPrimo 627) -- 4643 :o (chequeado con https://t5k.org/nthprime/index.php)

