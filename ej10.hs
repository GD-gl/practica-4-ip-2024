-- Serie geométrica
f1 :: Int -> Int
f1 n | n < 0 = undefined
     | otherwise = 2^(n+1) - 1 -- / (2 - 1)

-- Serie geométrica sin q^0 con q dado por usuario
f2 :: Int -> Float -> Float
f2 n q | n < 0 = undefined
       | n == 0 = 0
       | otherwise = ((q^(n+1) - 1) / (q - 1)) - 1 -- le resto el término en i=0 (=q^0 = 1)

f3 :: Int -> Float -> Float
-- f3 n q = f2 (n * 2) q
f3 n = f2 (n * 2) -- función currificada

f4 :: Int -> Float -> Float
-- de 1 a 2n - de 1 a n + caso n
f4 n q = f3 n q - f2 n q + q^n

main = do
    print (f1 10) -- 2047
    print (f1 4) -- 31
    print (f1 2) -- 7
    print (f1 3) -- 15
    print (f1 6) -- 127

    print (f2 10 2) -- 2046.0
    print (f2 4 2) -- 30.0
    print (f2 2 2) -- 6.0
    print (f2 3 2) -- 14.0
    print (f2 6 2) -- 126.0
    print (f2 10 6.5) -- 1.5e8
    print (f2 4 6.5) -- 2018.4
    print (f2 2 6.5) -- 48.75
    print (f2 3 6.5) -- 323.37
    print (f2 6 6.5) -- 89130
    print (f2 3 (-5)) -- -105.0

    print (f3 10 2) -- 2.09e6
    print (f3 4 2) -- 510.0
    print (f3 2 2) -- 30.0
    print (f3 3 2) -- 126.0
    print (f3 6 2) -- 8190.0
    print (f3 10 6.5) -- 2e16
    print (f3 4 6.5) -- 3.7e6
    print (f3 2 6.5) -- 2108.4
    print (f3 3 6.5) -- 89130.2
    print (f3 6 6.5) -- 6.7e9
    print (f3 3 (-5)) -- 13020.0

    print (f4 10 2) -- 2.09e6
    print (f4 4 2) -- 496.0
    print (f4 2 2) -- 28.0
    print (f4 3 2) -- 120.0
    print (f4 6 2) -- 8128.0