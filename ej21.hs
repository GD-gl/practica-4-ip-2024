-- "bottom-up approach", arranqué con lo que necesito para hacer mis bases y voy llegando a la función final

satisfaceUnoOCero :: Integer -> Integer -> Integer -> Integer
satisfaceUnoOCero p q r | (p^2) + (q^2) <= r^2 = 1
                        | otherwise = 0

-- para cada q de un p fijo se fija si satisface
satisfacenQ :: Integer -> Integer -> Integer -> Integer
satisfacenQ p 0 r = satisfaceUnoOCero p 0 r
satisfacenQ p q r = satisfaceUnoOCero p q r + satisfacenQ p (q-1) r

-- para cada p se fija cuantos q satisfacen
satisfacenPQ :: Integer -> Integer -> Integer -> Integer
satisfacenPQ 0 q r = satisfacenQ 0 q r
satisfacenPQ p q r = satisfacenQ p q r + satisfacenPQ (p-1) q r

pitagoras :: Integer -> Integer -> Integer -> Integer
pitagoras m n r | not (m >= 0 && n >= 0 && r >= 0) = undefined
                | otherwise = satisfacenPQ m n r

main = do
    print (pitagoras 3 4 5) -- 20
    print (pitagoras 3 4 2) -- 6