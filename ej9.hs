-- FUNCIONES DE PROBLEMA 8
cantDigitosInterno :: Int -> Int
cantDigitosInterno 0 = 1
cantDigitosInterno n = 1 + cantDigitosInterno (n `div` 10)

cantDigitos :: Int -> Int
cantDigitos n | n < 0 = undefined
              | n == 0 = 1
              | otherwise = cantDigitosInterno (n `div` 10)

iesimoDigito :: Int -> Int -> Int
iesimoDigito n i | n < 0 || i < 1 || i > cantDigitos n = undefined
                 | otherwise = (n `div` 10^(cantDigitos n - i)) `mod` 10


-- ejercicio 9:

esCapicuaInterno :: Int -> Int -> Bool
esCapicuaInterno n posCifra | posCifraOpuesta == posCifra = True -- la cantidad de cifras es impar
                         | posCifra > posCifraOpuesta = True -- se pasó
                         | otherwise = esCapicuaInterno n (posCifra+1) && (iesimoDigito n posCifra == iesimoDigito n posCifraOpuesta)
                         where nDig = cantDigitos n
                               posCifraOpuesta = nDig - posCifra + 1

esCapicua :: Int -> Bool
esCapicua n | n < 0 = undefined -- n >= 0
            | n `div` 10 == 0 = True
            | otherwise = esCapicuaInterno n 1

main = do
    print (esCapicua 0) -- True
    print (esCapicua 9) -- True
    print (esCapicua 4) -- True
    print (esCapicua 14) -- False
    print (esCapicua 11) -- True
    print (esCapicua 111) -- True
    print (esCapicua 151) -- True
    print (esCapicua 1511) -- False
    print (esCapicua 1551) -- True
    print (esCapicua 7654567) -- True
    print (esCapicua 7654566) -- False
    print (esCapicua 7644567) -- False