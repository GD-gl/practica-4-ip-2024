todosDigitosIgualesInterno :: Int -> Int -> Bool
todosDigitosIgualesInterno 0 primdigit = True
todosDigitosIgualesInterno n primdigit = todosDigitosIgualesInterno (n `div` 10) primdigit && (n `mod` 10 == primdigit)

todosDigitosIguales :: Int -> Bool
todosDigitosIguales n | n <= 0 = undefined
                      | otherwise = todosDigitosIgualesInterno n (n `mod` 10)

main = do
    print (todosDigitosIguales 1111) -- True
    print (todosDigitosIguales 66) -- True
    print (todosDigitosIguales 88888) -- True
    print (todosDigitosIguales 121111) -- False
    print (todosDigitosIguales 100000) -- False
    print (todosDigitosIguales 00001) -- True
    print (todosDigitosIguales 2543) -- False