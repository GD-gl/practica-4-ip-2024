-- n >= 0
fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib n | n >= 0 = fib (n-1) + fib(n-2)

main = do
    print (fib 10) -- 55
    print (fib 20) -- 6765
    print (fib 14) -- 377