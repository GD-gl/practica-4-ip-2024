-- resta un numero hasta que ya no se pueda restar más
-- N0 (naturales mas el cero)
esDivisiblePos :: Int -> Int -> Bool
esDivisiblePos n resto | n < resto = n == 0
                       | otherwise = esDivisiblePos (n - resto) resto

esDivisible :: Int -> Int -> Bool
esDivisible dividendo divisor = esDivisiblePos (abs dividendo) (abs divisor)

main = do
    print (esDivisible 2 4) -- False
    print (esDivisible 4 2) -- True
    print (esDivisible 1267832 2) -- True
    print (esDivisible 28 3) -- False
    print (esDivisible 33 3) -- True