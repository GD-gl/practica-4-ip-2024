-- n >= 1
aN :: Int -> Float
aN 1 = 2
aN n = 2 + (1.0 / aN (n - 1))

raizDe2Aprox :: Int -> Float
raizDe2Aprox n | n <= 0 = undefined
               | otherwise = aN n - 1

main = do
    print (raizDe2Aprox 20) -- 1.414213 (sqrt(2))
