-- CODIGO DEL EJERCICIO 16
menorDivisorInterno :: Integer -> Integer -> Integer
menorDivisorInterno n iter | iter < 2 = undefined -- 2 para adelante
                           | n == 1 = 1
                           | iter == n = n
                           | n `mod` iter == 0 = iter
                           | otherwise = menorDivisorInterno n (iter+1)

menorDivisor :: Integer -> Integer
menorDivisor n | n < 1 = undefined
               | otherwise = menorDivisorInterno n 2

esPrimo :: Integer -> Bool
esPrimo n = menorDivisor n == n

siguientePrimo :: Integer -> Integer
siguientePrimo n | esPrimo m = m
                 | otherwise = siguientePrimo m
                 where m = n + 1

nEsimoPrimoInterno :: Integer -> Integer -> Integer
nEsimoPrimoInterno previo iter | iter == 1 = siguientePrimo previo
                               | otherwise = nEsimoPrimoInterno (siguientePrimo previo) (iter - 1)

nEsimoPrimo :: Integer -> Integer
nEsimoPrimo n | n < 1 = undefined
              | otherwise = nEsimoPrimoInterno 1 n

-- Ejercicio 19:

esSumaInicialDePrimosInterno :: Integer -> Integer -> Integer -> Bool
esSumaInicialDePrimosInterno n primosSuma primoN | primosSuma > n = False
                                                 | primosSuma == n = True
                                                 | otherwise = esSumaInicialDePrimosInterno n (primosSuma + nEsimoPrimo (primoN + 1)) (primoN + 1)

esSumaInicialDePrimos :: Integer -> Bool
esSumaInicialDePrimos n | n < 0 = undefined
                        | otherwise = esSumaInicialDePrimosInterno n 2 1 -- primer primo = 2, primoN = 1

main = do
    print (esSumaInicialDePrimos 2) -- True
    print (esSumaInicialDePrimos 5) -- True
    print (esSumaInicialDePrimos 10) -- True
    print (esSumaInicialDePrimos 17) -- True
    print (esSumaInicialDePrimos 41) -- True
    print (esSumaInicialDePrimos 52) -- False
    print (esSumaInicialDePrimos 59) -- False
