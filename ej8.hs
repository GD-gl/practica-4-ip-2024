cantDigitosInterno :: Int -> Int
cantDigitosInterno 0 = 1
cantDigitosInterno n = 1 + cantDigitosInterno (n `div` 10)

cantDigitos :: Int -> Int
cantDigitos n | n < 0 = undefined
              | n == 0 = 1
              | otherwise = cantDigitosInterno (n `div` 10)

iesimoDigito :: Int -> Int -> Int
iesimoDigito n i | n < 0 || i < 1 || i > cantDigitos n = undefined
                 | otherwise = (n `div` 10^(cantDigitos n - i)) `mod` 10

main = do
    print (cantDigitos 1) -- 1
    print (cantDigitos 234) -- 3

    print (iesimoDigito 193 1) -- 1
    print (iesimoDigito 193 2) -- 9
    print (iesimoDigito 193 3) -- 3
    print (iesimoDigito 2089373 2) -- 0