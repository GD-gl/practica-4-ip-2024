-- m >= 1
sumatoriaAnidada :: Int -> Int -> Int
sumatoriaAnidada i m | m < 1 = undefined
                     | m == 1 = i
                     | otherwise = i^m + sumatoriaAnidada i (m-1)

sumatoria :: Int -> Int -> Int
sumatoria 0 m = 0 -- el caso 1 lo cubre el de abajo
sumatoria n m = sumatoriaAnidada n m + sumatoria (n-1) m

f :: Int -> Int -> Int
f n m | n < 1 || m < 1 = undefined
      | otherwise = sumatoria n m

main = do
    print (f 1 1) -- 1
    print (f 100 1) -- 1¹ + 2¹ + 3¹ ... basicamente la suma de gauss
    print (f 1 100) -- 100
    print (f 2 2) -- 1¹ + 1² + 2¹ + 2² = 8
    print (f 2 3) -- 1¹ + 1² + 1³ + 2¹ + 2² + 2³ = 17
    print (f 3 2) -- 1¹ + 1² + 2¹ + 2² + 3¹ + 3² = 20
    print (f 3 3) -- 1¹ + 1² + 1³ + 2¹ + 2² + 2³ + 3¹ + 3² + 3³ = 56