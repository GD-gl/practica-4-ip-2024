sumaRacionalesQ :: Int -> Int -> Float
sumaRacionalesQ p 1 = fromIntegral p
sumaRacionalesQ p q = (fromIntegral p / fromIntegral q) + sumaRacionalesQ p (q-1)

sumaRacionalesPQ :: Int -> Int -> Float
sumaRacionalesPQ 1 q = sumaRacionalesQ 1 q
sumaRacionalesPQ p q = sumaRacionalesPQ (p-1) q + sumaRacionalesQ p q

sumaRacionales :: Int -> Int -> Float
sumaRacionales p q | p < 1 || q < 1 = undefined
                   | otherwise = sumaRacionalesPQ p q

main = do
    print (sumaRacionales 3 2) -- 1/1 + 1/2 + 2/1 + 2/2 + 3/1 + 3/2 = 9.0
    print (sumaRacionales 3 9) -- 1/1 + 1/2 + 1/3 + 1/4 + 1/5 + 1/6 + 1/7 + 1/8 + 1/9 + 2/1 + 2/2 + 2/3 + 2/4 + 2/5 + 2/6 + 2/7 + 2/8 + 2/9 + 3/1 + 3/2 + 3/3 + 3/4 + 3/5 + 3/6 + 3/7 + 3/8 + 3/9 = 16.9 supongo
    print (sumaRacionales 4 2) -- 1/1 + 1/2 + 2/1 + 2/2 + 3/1 + 3/2 + 4/1 + 4/2 = 15.0