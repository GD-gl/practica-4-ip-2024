-- se puede usar 'floor'?
-- parteEntera r = floor r

parteDecimalMenosParteEntera :: Float -> Float
parteDecimalMenosParteEntera r | r < 1 = r
                               | otherwise = parteDecimalMenosParteEntera (r - 1)

parteEnteraFloat :: Float -> Float
parteEnteraFloat r = r - parteDecimalMenosParteEntera r

-- leer DUDAS.txt
parteEntera :: Float -> Int
parteEntera r = round (parteEnteraFloat r)

main = do
    print (parteEntera 1.1) -- 1
    print (parteEntera 10.9) -- 10