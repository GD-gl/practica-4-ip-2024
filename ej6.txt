problema sumaDigitos(a: N): N {
    requiere: {True}
    asegura: {res = sumatoria (i = 1 hasta round(log base 10 de a) + 1) {(a `div` 10^(i - 1)) `mod` 10}}
}

Obs: (round(log base 10 de a) + 1) es la cantidad de cifras que tiene a
Obs 2: uso round en vez de floor porque floor me puede dar errores de conversión por puntos flotantes
      (por ejemplo: logBase 10 1000 = 2.99999 en vez de 3)
Obs 3: en haskell no voy a usar logBase pero me sirvió para especificar el problema