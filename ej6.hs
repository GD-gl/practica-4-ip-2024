cantCifrasInterno :: Int -> Int
cantCifrasInterno 0 = 1
cantCifrasInterno n = 1 + cantCifrasInterno (n `div` 10)

cantCifras :: Int -> Int
cantCifras 0 = 1
cantCifras n = cantCifrasInterno (n `div` 10)

sumaDigitosInterno :: Int -> Int -> Int
sumaDigitosInterno _ 0 = 0
sumaDigitosInterno num cifra = sumaDigitosInterno num (cifra - 1) + ((num `div` 10^(cifra - 1)) `mod` 10)

sumaDigitos :: Int -> Int
sumaDigitos 0 = 0
sumaDigitos n | n < 0 = undefined -- no quiero enteros
              | otherwise = sumaDigitosInterno n (cantCifras n)

main = do

    print (cantCifras 10) -- 2
    print (cantCifras 1) -- 1
    print (cantCifras 0) -- 1
    print (cantCifras 0128) -- 3
    print (cantCifras 1028) -- 4

    print '-'

    print (sumaDigitos 1) -- 1
    print (sumaDigitos 11) -- 2
    print (sumaDigitos 111) -- 3
    print (sumaDigitos 1111) -- 4
    print (sumaDigitos 11111) -- 5
    print (sumaDigitos 22222) -- 10
    print (sumaDigitos 873469) -- 37

